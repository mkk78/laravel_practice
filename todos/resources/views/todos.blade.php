@extends('layout')

@section('content')

    <div class="row">
        <div class="col-lg-6 col-lg-offset-3">
            <form action="/create/todo" method="post">
                {{csrf_field()}}
                <input class="form-control input-lg" placeholder="Create a new todo" name="todo"></input>
            </form>
        </div>
    </div>

    @foreach($todos as $todo)
        {{ $todo->todo }} <a href="{{route('todo.delete',['id' => $todo->id]) }}" class="btn btn-danger">Delete</a>
        <a href="{{route('todo.update',['id' => $todo->id]) }}" class="btn btn-success btn-xs">Update</a>

        @if(!$todo->completed)
            <a href="{{route('todos.completed',['id' => $todo->id]) }}" class="btn btn-xs btn-info">Mark as completed</a>
        @endif
        <hr>
    @endforeach

@stop